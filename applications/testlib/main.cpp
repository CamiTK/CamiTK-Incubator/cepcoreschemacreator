#include <QtTest/QtTest>
#include "unit_tests_Qt.h"
//QTEST_MAIN(TestCep)
//QTEST_MAIN(TestActionExtension)

int main(int argc, char *argv[]) \
{QStringList testCmd;
QDir testLogDir;
testLogDir.mkdir("UnitTest_Results");
testCmd << " " << "-o" << "UnitTest_Results/test_log.txt";

TestCep mTestCep;
int res = QTest::qExec(&mTestCep, testCmd);

TestActionExtension mTestActionExt;
res += QTest::qExec(&mTestActionExt, testCmd);

TestAction mTestAction;
res += QTest::qExec(&mTestAction, testCmd);

TestComponentExtension mTestComponentExt;
res += QTest::qExec(&mTestComponentExt, testCmd);

TestComponent mTestComponent;
res += QTest::qExec(&mTestComponent, testCmd);

TestLibrary mTestLibrary;
res += QTest::qExec(&mTestLibrary, testCmd);

return res;
}
