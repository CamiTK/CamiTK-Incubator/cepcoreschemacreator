#include "CamiTK2CepCoreSchemaAPI.h"
#include "qstring.h"

namespace cepcoreschema
{
	class Parameter;
}
class CAMITK2CEPCORESCHEMA_API ParameterC2ParameterS
{
public:
	ParameterC2ParameterS();
	~ParameterC2ParameterS();

	/**
	Get the cepcoreschema::Parameter converted by the converter.
	*/
	cepcoreschema::Parameter* getCepCoreSchemaParameter()
	{
		return mDomParameter;
	}

	/**
	Fill the field name in the dom Tree
	*/
	void setName(QString str);

	/**
	Fill the field description in the dom Tree
	*/
	void setDescription(QString str);

	/**
	Fill the field type in the dom Tree
	*/
	void setType(QString str);

	/**
	Fill the field default value in the dom Tree
	*/
	void setDefaultValue(QString str);

	/**
	Fill the field unit in the dom Tree
	*/
	void setUnit(QString str);

	/**
	Fill the field editable in the dom Tree with the value of b
	*/
	void setEditable(bool b);

private:
	cepcoreschema::Parameter* mDomParameter;
};

