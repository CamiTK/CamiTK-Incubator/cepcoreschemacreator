#include "LibraryC2LibraryS.h"
#include "DependencyC2DependencyS.h"
#include "Library.hxx"


LibraryC2LibraryS::LibraryC2LibraryS()
{
	mDomLibrary = new cepcoreschema::Library("", "");
}


LibraryC2LibraryS::~LibraryC2LibraryS()
{
	delete mDomLibrary;
}

void LibraryC2LibraryS::setName(QString str)
{
	mDomLibrary->name(str.toStdString());
}

void LibraryC2LibraryS::setDescription(QString str)
{
	mDomLibrary->description(str.toStdString());
}

void LibraryC2LibraryS::addDependency(DependencyC2DependencyS* dep)
{
	if (mDomLibrary->dependencies().present())
	{
		mDomLibrary->dependencies()->dependency().push_back(*dep->getCepCoreSchemaDependency());
	}
	else
	{
		cepcoreschema::Dependencies * domDependencies = new cepcoreschema::Dependencies();
		domDependencies->dependency().push_back(*dep->getCepCoreSchemaDependency());
		mDomLibrary->dependencies(*domDependencies);
	}
	delete dep;
}

void LibraryC2LibraryS::removeAllDependencies()
{
	if (mDomLibrary->dependencies().present())
	{
		mDomLibrary->dependencies()->dependency().clear();
	}
}