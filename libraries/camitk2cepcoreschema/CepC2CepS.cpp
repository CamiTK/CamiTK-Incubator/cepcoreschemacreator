#include "CepC2CepS.h"

#include "ActionExtC2ActionExtS.h"
#include "ComponentExtC2ComponentExtS.h"
#include "LibraryC2LibraryS.h"
#include "ApplicationC2ApplicationS.h"

#include <Cep.hxx>


CepC2CepS::CepC2CepS()
{
	mDomCep = NULL;
	mDomCep = new cepcoreschema::Cep("", cepcoreschema::Contact(), "");

}


CepC2CepS::~CepC2CepS()
{
	if (mDomCep)
	{
		delete mDomCep;
	}
}

void CepC2CepS::setName(QString str)
{
	mDomCep->name(str.toStdString());
}

QString CepC2CepS::getName()
{
	return mDomCep->name().c_str();
}

void CepC2CepS::setDescription(QString str)
{
	mDomCep->description(str.toStdString());
}

void CepC2CepS::setReadMe(QString str)
{
	mDomCep->readme(str.toStdString());
}

void CepC2CepS::setContact(QString str)
{
	cepcoreschema::Email mail(str.toStdString());
	cepcoreschema::Contact::email_sequence & mailSeq(mDomCep->contact().email());
	mailSeq.clear();
	mailSeq.push_back(mail);
}

void CepC2CepS::setContact(QStringList strList)
{
	removeAllContact();
	for (int i = 0; i < strList.size(); i++)
	{
		addContact(strList.at(i));
	}
}

void CepC2CepS::addContact(QString str)
{
	cepcoreschema::Email mail(str.toStdString());
	cepcoreschema::Contact::email_sequence & mailSeq(mDomCep->contact().email());
	mailSeq.push_back(mail);
}

void CepC2CepS::removeAllContact()
{
	cepcoreschema::Contact::email_sequence & mailSeq(mDomCep->contact().email());
	mailSeq.clear();
}

void CepC2CepS::setLicence(QString str)
{
	mDomCep->copyright(str.toStdString());

}

void CepC2CepS::addActionExtension(ActionExtC2ActionExtS* actExt)
{
	addActionExtension(actExt->getCepCoreSchemaActionExtension());
	delete actExt;
}

void CepC2CepS::addActionExtension(cepcoreschema::ActionExtension* actExt)
{
	if (mDomCep->actionExtensions().present())
	{
		mDomCep->actionExtensions().get().actionExtension().push_back(*actExt);
	}
	else
	{
		cepcoreschema::ActionExtensions theExtensions;
		theExtensions.actionExtension().push_back((*actExt));
		mDomCep->actionExtensions(theExtensions);
	}
}

void CepC2CepS::removeAllActionExtension()
{
	if (mDomCep->actionExtensions().present())
	{
		mDomCep->actionExtensions().reset();
	}
}

void CepC2CepS::removeActionExtension(cepcoreschema::ActionExtension* actExt)
{
	if (mDomCep->actionExtensions().present())
	{
		// mDomCep->actionExtensions().get().actionExtension().erase(*actExt);
	}
}

void CepC2CepS::addComponentExtension(ComponentExtC2ComponentExtS* compExt)
{
	addComponentExtension(compExt->getCepCoreSchemaComponentExtension());
	delete compExt;
}

void CepC2CepS::addComponentExtension(cepcoreschema::ComponentExtension* compExt)
{
	if (mDomCep->componentExtensions().present())
	{
		mDomCep->componentExtensions().get().componentExtension().push_back(*compExt);
	}
	else
	{
		cepcoreschema::ComponentExtensions theExtensions;
		theExtensions.componentExtension().push_back((*compExt));
		mDomCep->componentExtensions(theExtensions);
	}
}

void CepC2CepS::removeAllComponentExtension()
{
	if (mDomCep->componentExtensions().present())
	{
		mDomCep->componentExtensions().reset();
	}
}

void CepC2CepS::removeComponentExtension(cepcoreschema::ComponentExtension* compExt)
{
	if (mDomCep->componentExtensions().present())
	{
		//mDomCep->componentExtensions().get().componentExtension().erase(*compExt);
	}
}

void CepC2CepS::addLibrary(LibraryC2LibraryS* lib)
{
	addLibrary(lib->getCepCoreSchemaLibrary());
	delete lib;
}


void CepC2CepS::addLibrary(cepcoreschema::Library* lib)
{
	if (mDomCep->libraries().present())
	{
		mDomCep->libraries().get().library().push_back(*lib);
	}
	else
	{
		cepcoreschema::Libraries libs;
		libs.library().push_back((*lib));
		mDomCep->libraries(libs);
	}
}


void CepC2CepS::removeAllLibraries()
{
	if (mDomCep->libraries().present())
	{
		mDomCep->libraries().reset();
	}
}

void CepC2CepS::removeLibrary(cepcoreschema::Library* lib)
{
	if (mDomCep->libraries().present())
	{
		//mDomCep->libraries().get().library().erase(*lib);
	}
}


void CepC2CepS::addApplication(ApplicationC2ApplicationS* app)
{
	addApplication(app->getCepCoreSchemaApplication());
	delete app;
}


void CepC2CepS::addApplication(cepcoreschema::Application* app)
{
	if (mDomCep->applications().present())
	{
		mDomCep->applications().get().application().push_back(*app);
	}
	else
	{
		cepcoreschema::Applications apps;
		apps.application().push_back((*app));
		mDomCep->applications(apps);
	}
}


void CepC2CepS::removeAllApplications()
{
	if (mDomCep->applications().present())
	{
		mDomCep->applications().reset();
	}
}


void CepC2CepS::removeApplication(cepcoreschema::Application* app)
{
	if (mDomCep->applications().present())
	{
		//mDomCep->applications().get().application().erase(*app);
	}
}