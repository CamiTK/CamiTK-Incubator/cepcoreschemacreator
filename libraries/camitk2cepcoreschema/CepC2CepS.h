// Dependency from Qt
#include <qstringlist.h>

#include "CamiTK2CepCoreSchemaAPI.h"

// Dependency from cepcoreschema
namespace cepcoreschema
{
	class Cep;
	class ActionExtension;
	class ComponentExtension;
	class Library;
	class Application;
}

class ActionExtC2ActionExtS;
class ComponentExtC2ComponentExtS;
class LibraryC2LibraryS;
class ApplicationC2ApplicationS;


class CAMITK2CEPCORESCHEMA_API CepC2CepS
{
	// TODO: manage the components, libraries, applications.

public:
	CepC2CepS();
	~CepC2CepS();

	/**
	* Get the cepcoreschema::cep converted by the converter.
	*/
	cepcoreschema::Cep* const getCepCoreSchemaCep() const
	{
		return mDomCep;
	}

	/**
	* Set the cepcoreschema::cep if it already exists. This operation erase all modifications that can have been done.
	*/
	void setCepCoreSchemaCep(cepcoreschema::Cep*cep)
	{
		mDomCep = cep;
	}

	/**
	* Fill the field name in the dom Tree
	*/
	void setName(QString str);

	/**
	* Get the field name in the dom Tree
	*/
	QString getName();

	/**
	* Fill the field description in the dom Tree
	*/
	void setDescription(QString str);

	/**
	* Fill the field readme in the dom Tree
	*/
	void setReadMe(QString str);

	/**
	* Fill the field contact in the dom Tree
	*/
	void setContact(QString str);

	/**
	* Add a contact to the existing list of contact in the field contact in the dom Tree
	*/
	void addContact(QString str);

	/**
	* Fill the field contact in the dom Tree
	*/
	void setContact(QStringList strList);

	/**
	* Remove all contact in the field contact in the dom Tree
	*/
	void removeAllContact();

	/**
	* Fill the licence field in the dom tree
	*/
	void setLicence(QString str);

	/**
	* Fill the actionExtension field in the dom Tree
	*/
	void addActionExtension(ActionExtC2ActionExtS* actExt);

	/**
	* Fill the actionExtension field in dom Tree
	*/
	void addActionExtension(cepcoreschema::ActionExtension* actExt);

	/**
	* Remove all actionExtensions in the field actionExtension in the domm Tree
	*/
	void removeAllActionExtension();

	/**
	* TODO !
	* Remove the selected actionExtension in the field actionExtension in the dom Tree
	*/
	void removeActionExtension(cepcoreschema::ActionExtension* actExt);

	/**
	* Fill the componentExtension field in the dom Tree
	*/
	void addComponentExtension(ComponentExtC2ComponentExtS* compExt);

	/**
	* Fill the componentExtension field in dom Tree
	*/
	void addComponentExtension(cepcoreschema::ComponentExtension* compExt);

	/**
	* Remove all componentExtension in the field actionExtension in the domm Tree
	*/
	void removeAllComponentExtension();

	/**
	* TODO !
	* Remove the selected componentExtension in the field componentExtension in the dom Tree
	*/
	void removeComponentExtension(cepcoreschema::ComponentExtension* compExt);

	/**
	* Fill the library field in the dom Tree
	*/
	void addLibrary(LibraryC2LibraryS* lib);

	/**
	* Fill the library field in dom Tree
	*/
	void addLibrary(cepcoreschema::Library* lib);

	/**
	* Remove all libraries in the field actionExtension in the domm Tree
	*/
	void removeAllLibraries();

	/**
	* TODO !
	* Remove the selected library in the field componentExtension in the dom Tree
	*/
	void removeLibrary(cepcoreschema::Library* lib);

	/**
	* Fill the Application field in the dom Tree
	*/
	void addApplication(ApplicationC2ApplicationS* app);

	/**
	* Fill the Application field in dom Tree
	*/
	void addApplication(cepcoreschema::Application* app);

	/**
	* Remove all applications in the field actionExtension in the domm Tree
	*/
	void removeAllApplications();

	/**
	* TODO !
	* Remove the selected Application in the field componentExtension in the dom Tree
	*/
	void removeApplication(cepcoreschema::Application* app);

private:
	cepcoreschema::Cep* mDomCep;
};


