#include "ComponentExtC2ComponentExtS.h"
#include "ComponentC2ComponentS.h"
#include "DependencyC2DependencyS.h"

#include "ComponentExtension.hxx"


ComponentExtC2ComponentExtS::ComponentExtC2ComponentExtS()
{
	mDomComponentExtension = new cepcoreschema::ComponentExtension("", "", cepcoreschema::Components());
}


ComponentExtC2ComponentExtS::~ComponentExtC2ComponentExtS()
{
	delete mDomComponentExtension;
}

void ComponentExtC2ComponentExtS::setName(QString str)
{
	mDomComponentExtension->name(str.toStdString());
}

void ComponentExtC2ComponentExtS::setDescription(QString str)
{
	mDomComponentExtension->description(str.toStdString());
}

void ComponentExtC2ComponentExtS::addComponent(ComponentC2ComponentS* comp)
{
	mDomComponentExtension->components().component().push_back(*comp->getCepCoreSchemaComponent());
	delete comp;
}

void ComponentExtC2ComponentExtS::addDependency(DependencyC2DependencyS* dep)
{
	if (mDomComponentExtension->dependencies().present())
	{
		mDomComponentExtension->dependencies()->dependency().push_back(*dep->getCepCoreSchemaDependency());
	}
	else
	{
		cepcoreschema::Dependencies * domDependencies = new cepcoreschema::Dependencies();
		domDependencies->dependency().push_back(*dep->getCepCoreSchemaDependency());
		mDomComponentExtension->dependencies(*domDependencies);
	}
	delete dep;
}

void ComponentExtC2ComponentExtS::removeAllDependencies()
{
	if (mDomComponentExtension->dependencies().present())
	{
		mDomComponentExtension->dependencies()->dependency().clear();
	}
}

void ComponentExtC2ComponentExtS::removeAllComponents()
{
	mDomComponentExtension->components().component().clear();
}
