#include "CamiTK2CepCoreSchemaAPI.h"

#include "qstring.h"

// Dependency from cepcoreschema
namespace cepcoreschema
{
	class Action;
}

class ParameterC2ParameterS;

class CAMITK2CEPCORESCHEMA_API ActionC2ActionS
{
public:
	ActionC2ActionS();
	~ActionC2ActionS();

	/**
	Get the cepcoreschema::Action converted by the converter.
	*/
	cepcoreschema::Action* getCepCoreSchemaAction()
	{
		return mDomAction;
	}

	/**
	Fill the field name in the dom Tree
	*/
	void setName(QString str);

	/**
	Fill the field description in the dom Tree
	*/
	void setDescription(QString str);

	/**
	Fill the field component (ie: the component on wich the action is applied) in the dom Tree
	*/
	void setComponent(QString str);

	/**
	Fill the field classification with ITKFilter and add its outputType
	*/
	void setITKFilter(QString outputType);

	/**
	Fill the field classification with the family
	*/
	void setFamily(QString str);

	/**
	Fill the field classification with the tag str
	*/
	void addTag(QString str);

	/**
	Remove the tag str
	TODO: to be implemented
	*/
	void removeTag(QString str);

	/**
	Remove all tags
	*/
	void removeAllTags();

	/**
	Fill the field parameters with the parameter designed.
	*/
	void addParameter(ParameterC2ParameterS* param);

	/**
	Remove all parameters in the field parameters.
	*/
	void removeAllParameters();


private:
	cepcoreschema::Action* mDomAction;
};

