#include "DependencyC2DependencyS.h"

#include "Dependency.hxx"


DependencyC2DependencyS::DependencyC2DependencyS()
{
	mDomDependency = new cepcoreschema::Dependency("","");
}


DependencyC2DependencyS::~DependencyC2DependencyS()
{
	delete mDomDependency;
}


void DependencyC2DependencyS::setType(QString str)
{
	mDomDependency->type(str.toStdString());
}

void DependencyC2DependencyS::setName(QString str)
{
	mDomDependency->name(str.toStdString());
}

void DependencyC2DependencyS::setAdditional(QString str)
{
	mDomDependency->additional(str.toStdString());
}
