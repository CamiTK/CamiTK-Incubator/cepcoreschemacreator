#include "CamiTK2CepCoreSchemaAPI.h"

// Dependency from Qt
#include <qstring.h>

// Dependency from cepcoreschema
namespace cepcoreschema
{
	class Component;
}

class ParameterC2ParameterS;

class CAMITK2CEPCORESCHEMA_API ComponentC2ComponentS
{
public:
	ComponentC2ComponentS();
	~ComponentC2ComponentS();

	/**
	Get the cepcoreschema::Component converted by the converter.
	*/
	cepcoreschema::Component* getCepCoreSchemaComponent()
	{
		return mDomComponent;
	}

	/**
	Fill the field name in the dom Tree
	*/
	void setName(QString str);

	/**
	Fill the field description in the dom Tree
	*/
	void setDescription(QString str);

	/**
	Fill the field fileSuffix in the dom Tree
	*/
	void setFileSuffix(QString str);

	/**
	Fill the field representation in the dom Tree
	*/
	void setRepresentation(QString str);

	/**
	Fill the field properties with the parameter designed.
	*/
	void addProperty(ParameterC2ParameterS* param);

	/**
	Remove all properties in the field parameters.
	*/
	void removeAllProperties();

private:
	cepcoreschema::Component* mDomComponent;
};

