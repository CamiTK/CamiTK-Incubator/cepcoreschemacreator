#include "ActionExtC2ActionExtS.h"
#include "ActionC2ActionS.h"
#include "DependencyC2DependencyS.h"

#include "ActionExtension.hxx"


ActionExtC2ActionExtS::ActionExtC2ActionExtS()
{
	mDomActionExtension = NULL;

	mDomActionExtension = new cepcoreschema::ActionExtension("", "", cepcoreschema::Actions());

}


ActionExtC2ActionExtS::~ActionExtC2ActionExtS()
{
	delete mDomActionExtension;
}

void ActionExtC2ActionExtS::setName(QString str)
{
	mDomActionExtension->name(str.toStdString());
}

void ActionExtC2ActionExtS::setDescription(QString str)
{
	mDomActionExtension->description(str.toStdString());
}

void ActionExtC2ActionExtS::addAction(ActionC2ActionS* act)
{
	mDomActionExtension->actions().action().push_back(*act->getCepCoreSchemaAction());
	delete act;
}

void ActionExtC2ActionExtS::addDependency(DependencyC2DependencyS* dep)
{
	if (mDomActionExtension->dependencies().present())
	{
		mDomActionExtension->dependencies()->dependency().push_back(*dep->getCepCoreSchemaDependency());
	}
	else
	{
		cepcoreschema::Dependencies * domDependencies = new cepcoreschema::Dependencies();
		domDependencies->dependency().push_back(*dep->getCepCoreSchemaDependency());
		mDomActionExtension->dependencies(*domDependencies);
	}
	delete dep;
}

void ActionExtC2ActionExtS::removeAllDependencies()
{
	if (mDomActionExtension->dependencies().present())
	{
		mDomActionExtension->dependencies()->dependency().clear();
	}
}

void ActionExtC2ActionExtS::removeAllActions()
{
	mDomActionExtension->actions().action().clear();
}