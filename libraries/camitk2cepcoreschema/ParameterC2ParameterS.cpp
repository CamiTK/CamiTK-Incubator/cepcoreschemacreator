#include "ParameterC2ParameterS.h"

#include "Parameter.hxx"


ParameterC2ParameterS::ParameterC2ParameterS()
{
	mDomParameter = NULL;
	mDomParameter = new cepcoreschema::Parameter("", "", "");
}


ParameterC2ParameterS::~ParameterC2ParameterS()
{
	delete mDomParameter;
}


void ParameterC2ParameterS::setName(QString str)
{
	mDomParameter->name(str.toStdString());
}


void ParameterC2ParameterS::setDescription(QString str)
{
	mDomParameter->description(str.toStdString());
}


void ParameterC2ParameterS::setType(QString str)
{
	mDomParameter->type(str.toStdString());
}


void ParameterC2ParameterS::setDefaultValue(QString str)
{
	mDomParameter->defaultValue(str.toStdString());
}


void ParameterC2ParameterS::setUnit(QString str)
{
	mDomParameter->unit(str.toStdString());
}

void ParameterC2ParameterS::setEditable(bool b)
{
	mDomParameter->editable(b);
}