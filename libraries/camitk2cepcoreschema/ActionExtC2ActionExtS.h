// Dependency from Qt
#include <qstring.h>

#include "CamiTK2CepCoreSchemaAPI.h"

// Dependency from cepcoreschema
namespace cepcoreschema
{
	class ActionExtension;
}
class ActionC2ActionS;
class DependencyC2DependencyS;

class CAMITK2CEPCORESCHEMA_API ActionExtC2ActionExtS
{
	// TODO: manage the dependencies !
public:
	ActionExtC2ActionExtS();
	~ActionExtC2ActionExtS();

	/**
	Get the cepcoreschema::ActionExtension converted by the converter.
	*/
	cepcoreschema::ActionExtension * getCepCoreSchemaActionExtension()
	{
		return mDomActionExtension;
	}

	/**
	Fill the field name in the dom Tree
	*/
	void setName(QString str);

	/**
	Fill the field description in the dom Tree
	*/
	void setDescription(QString str);

	/**
	Fill the field action in the dom Tree
	*/
	void addAction(ActionC2ActionS* action);

	/**
	Fill the field dependencies in the dom Tree
	*/
	void addDependency(DependencyC2DependencyS* dependency);

	/**
	Remove all dependencies in the dom tree
	*/
	void removeAllDependencies();

	/**
	Remove all actions in the dom tree
	*/
	void removeAllActions();


private:
	cepcoreschema::ActionExtension * mDomActionExtension;
};

