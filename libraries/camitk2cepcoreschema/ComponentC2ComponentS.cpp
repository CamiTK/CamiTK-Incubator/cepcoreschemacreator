#include "ComponentC2ComponentS.h"
#include "ParameterC2ParameterS.h"

#include "Component.hxx"


ComponentC2ComponentS::ComponentC2ComponentS()
{
	mDomComponent = new cepcoreschema::Component("","","");
}


ComponentC2ComponentS::~ComponentC2ComponentS()
{
	delete mDomComponent;
}

void ComponentC2ComponentS::setName(QString str)
{
	mDomComponent->name(str.toStdString());
}


void ComponentC2ComponentS::setDescription(QString str)
{
	mDomComponent->description(str.toStdString());
}


void ComponentC2ComponentS::setFileSuffix(QString str)
{
	mDomComponent->fileSuffix().push_back(str.toStdString());
}

void ComponentC2ComponentS::setRepresentation(QString str)
{
	mDomComponent->representation(str.toStdString());
}


void ComponentC2ComponentS::addProperty(ParameterC2ParameterS* param)
{
	if (mDomComponent->properties().present())
	{
		mDomComponent->properties()->parameter().push_back(*param->getCepCoreSchemaParameter());
	}
	else
	{
		cepcoreschema::Parameters domParameters;
		cepcoreschema::Parameter parameter(*param->getCepCoreSchemaParameter());
		domParameters.parameter().push_back(parameter);
		mDomComponent->properties(domParameters);
	}
	delete param;
}

void ComponentC2ComponentS::removeAllProperties()
{
	if (mDomComponent->properties().present())
	{
		mDomComponent->properties()->parameter().clear();
	}
}
