#include "CamiTK2CepCoreSchemaAPI.h"

#include "qstring.h"

// Dependency from cepcoreschema
namespace cepcoreschema
{
	class ComponentExtension;
}

class ComponentC2ComponentS;
class DependencyC2DependencyS;

class CAMITK2CEPCORESCHEMA_API ComponentExtC2ComponentExtS
{
public:
	ComponentExtC2ComponentExtS();
	~ComponentExtC2ComponentExtS();

	/**
	Get the cepcoreschema::ComponentExtension converted by the converter.
	*/
	cepcoreschema::ComponentExtension* getCepCoreSchemaComponentExtension()
	{
		return mDomComponentExtension;
	}

	/**
	Fill the field name in the dom Tree
	*/
	void setName(QString str);

	/**
	Fill the field description in the dom Tree
	*/
	void setDescription(QString str);

	/**
	Fill the field component in the dom Tree
	*/
	void addComponent(ComponentC2ComponentS* component);

	/**
	Fill the field dependencies in the dom Tree
	*/
	void addDependency(DependencyC2DependencyS* dependency);

	/**
	Remove all dependencies in the dom tree
	*/
	void removeAllDependencies();

	/**
	Remove all components in the dom tree
	*/
	void removeAllComponents();

private:
	cepcoreschema::ComponentExtension* mDomComponentExtension;
};

