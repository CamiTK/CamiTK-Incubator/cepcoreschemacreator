#include "ApplicationC2ApplicationS.h"
#include "Application.hxx"


ApplicationC2ApplicationS::ApplicationC2ApplicationS()
{
	mDomApplication = new cepcoreschema::Application("", "", "");
}


ApplicationC2ApplicationS::~ApplicationC2ApplicationS()
{
	delete mDomApplication;
}

void ApplicationC2ApplicationS::setName(QString str)
{
	mDomApplication->name(str.toStdString());
}

void ApplicationC2ApplicationS::setDescription(QString str)
{
	mDomApplication->description(str.toStdString());

}

void ApplicationC2ApplicationS::setDirectoryName(QString str)
{
	mDomApplication->directoryName(str.toStdString());
}
