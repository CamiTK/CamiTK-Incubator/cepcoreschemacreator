#include "ActionC2ActionS.h"
#include "ParameterC2ParameterS.h"

#include "Action.hxx"



ActionC2ActionS::ActionC2ActionS()
{
	mDomAction = NULL;
	mDomAction = new cepcoreschema::Action("", "", "", cepcoreschema::Classification(""));
}


ActionC2ActionS::~ActionC2ActionS()
{
	delete mDomAction;
}


void ActionC2ActionS::setName(QString str)
{
	mDomAction->name(str.toStdString());
}


void ActionC2ActionS::setDescription(QString str)
{
	mDomAction->description(str.toStdString());
}


void ActionC2ActionS::setComponent(QString str)
{
	mDomAction->component(str.toStdString());
}


void ActionC2ActionS::setITKFilter(QString outputType)
{
	cepcoreschema::ItkFilter theFilter(outputType.toStdString());
	mDomAction->classification().itkFilter(theFilter);
}

void ActionC2ActionS::setFamily(QString str)
{
	mDomAction->classification().family(str.toStdString());
}

void ActionC2ActionS::addTag(QString str)
{
	mDomAction->classification().tag().push_back(str.toStdString());
}

void ActionC2ActionS::removeAllTags()
{
	mDomAction->classification().tag().clear();
}

void ActionC2ActionS::removeTag(QString str)
{

}

void ActionC2ActionS::addParameter(ParameterC2ParameterS* param)
{
	if (mDomAction->parameters().present())
	{
		mDomAction->parameters()->parameter().push_back(*param->getCepCoreSchemaParameter());
	}
	else
	{
		cepcoreschema::Parameters domParameters;
		cepcoreschema::Parameter parameter(*param->getCepCoreSchemaParameter());
		domParameters.parameter().push_back(parameter);
		mDomAction->parameters(domParameters);
	}
	delete param;
}

void ActionC2ActionS::removeAllParameters()
{
	if (mDomAction->parameters().present())
	{
		mDomAction->parameters()->parameter().clear();
	}
}
