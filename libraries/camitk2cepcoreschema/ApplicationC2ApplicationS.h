#include "CamiTK2CepCoreSchemaAPI.h"

#include "qstring.h"

// Dependency from cepcoreschema
namespace cepcoreschema
{
	class Application;
}

class CAMITK2CEPCORESCHEMA_API ApplicationC2ApplicationS
{
public:
	ApplicationC2ApplicationS();
	~ApplicationC2ApplicationS();

	/**
	Get the cepcoreschema::Application converted by the converter.
	*/
	cepcoreschema::Application* getCepCoreSchemaApplication()
	{
		return mDomApplication;
	}

	/**
	Fill the field name in the dom Tree
	*/
	void setName(QString str);

	/**
	Fill the field description in the dom Tree
	*/
	void setDescription(QString str);

	/**
	Fill the field directoryName in the dom Tree
	*/
	void setDirectoryName(QString str);

private:
	cepcoreschema::Application* mDomApplication;
};

