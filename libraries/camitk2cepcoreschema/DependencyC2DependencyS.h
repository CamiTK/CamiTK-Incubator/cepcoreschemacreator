#include "CamiTK2CepCoreSchemaAPI.h"
#include "qstring.h"

// Dependency from cepcoreschema
namespace cepcoreschema
{
	class Dependency;
}

class CAMITK2CEPCORESCHEMA_API DependencyC2DependencyS
{
public:
	DependencyC2DependencyS();
	~DependencyC2DependencyS();

	/**
	Get the cepcoreschema::Dependency converted by the converter.
	*/
	cepcoreschema::Dependency* getCepCoreSchemaDependency()
	{
		return mDomDependency;
	}

	/**
	Fill the field type in the dom Tree
	*/
	void setType(QString type);

	/**
	Fill the field name in the dom Tree
	*/
	void setName(QString name);

	/**
	Fill the field additional in the dom Tree
	*/
	void setAdditional(QString additional);

private:
	cepcoreschema::Dependency* mDomDependency;
};

