#include "CamiTK2CepCoreSchemaAPI.h"

#include "qstring.h"

// Dependency from cepcoreschema
namespace cepcoreschema
{
	class Library;
}

class DependencyC2DependencyS;

class CAMITK2CEPCORESCHEMA_API LibraryC2LibraryS
{
public:
	LibraryC2LibraryS();
	~LibraryC2LibraryS();

	/**
	Get the cepcoreschema::Library converted by the converter.
	*/
	cepcoreschema::Library* getCepCoreSchemaLibrary()
	{
		return mDomLibrary;
	}

	/**
	Fill the field name in the dom Tree
	*/
	void setName(QString str);

	/**
	Fill the field description in the dom Tree
	*/
	void setDescription(QString str);

	/**
	Fill the field dependencies in the dom Tree
	*/
	void addDependency(DependencyC2DependencyS* dependency);

	/**
	Remove all dependencies in the dom tree
	*/
	void removeAllDependencies();

private:
	cepcoreschema::Library* mDomLibrary;
};

