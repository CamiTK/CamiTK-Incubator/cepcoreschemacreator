#include "unit_tests_Qt.h"

#include "CepC2CepS.h"
#include "ActionExtC2ActionExtS.h"
#include "ComponentExtC2ComponentExtS.h"
#include "LibraryC2LibraryS.h"
#include "ApplicationC2ApplicationS.h"
#include "DependencyC2DependencyS.h"
#include "ActionC2ActionS.h"
#include "ParameterC2ParameterS.h"
#include "ComponentC2ComponentS.h"
#include "Cep.hxx"

/**************************************************************************************************
 ******************** TestCep *********************************************************************
 **************************************************************************************************/
TestCep::TestCep()
{
}

TestCep::~TestCep()
{
}

void TestCep::setName()
{
	CepC2CepS* cep = new CepC2CepS();
	QString name = "A first CEP";
	cep->setName(name);

	QVERIFY(name == cep->getCepCoreSchemaCep()->name().c_str());

}

void TestCep::setDescription()
{
	CepC2CepS* cep = new CepC2CepS();
	QString str = "CEP description";
	cep->setDescription(str);

	QVERIFY(str == cep->getCepCoreSchemaCep()->description().c_str());

}

void TestCep::setReadMe()
{
	CepC2CepS* cep = new CepC2CepS();
	QString str = "ReadMe";
	cep->setReadMe(str);

	QVERIFY(str == cep->getCepCoreSchemaCep()->readme().get().c_str());
}

void TestCep::setContact()
{
	CepC2CepS* cep = new CepC2CepS();
	QString str = "titi.toto@imag.fr";
	cep->setContact(str);

	QVERIFY(str == cep->getCepCoreSchemaCep()->contact().email().at(0).c_str());
}

void TestCep::addContact()
{
	CepC2CepS* cep = new CepC2CepS();
	QString str = "bili.toto@imag.fr";
	cep->addContact(str);

	QVERIFY(str == cep->getCepCoreSchemaCep()->contact().email().back().c_str());
}

void TestCep::removeAllContact()
{
	CepC2CepS* cep = new CepC2CepS();
	QString str = "bili.toto@imag.fr";
	cep->addContact(str);
	cep->removeAllContact();

	QVERIFY(0 == cep->getCepCoreSchemaCep()->contact().email().size());
}

void TestCep::setContactList()
{
	CepC2CepS* cep = new CepC2CepS();
	QStringList str("bili.toto@imag.fr");
	str.append("tata.tutu@imag.fr");
	str.append("polo.pili@imag.fr");
	cep->setContact(str);

	QVERIFY(str.size() == cep->getCepCoreSchemaCep()->contact().email().size());
}

void TestCep::setLicense()
{
	CepC2CepS* cep = new CepC2CepS();
	QString str = "This is the licence";
	cep->setLicence(str);

	QVERIFY(str == cep->getCepCoreSchemaCep()->copyright().get().c_str());
}

void TestCep::addActionExtension()
{
	CepC2CepS* cep = new CepC2CepS();
	ActionExtC2ActionExtS* actExt = new ActionExtC2ActionExtS();
	QString name = "name of the extension";
	actExt->setName(name);

	cep->addActionExtension(actExt);
	
	QVERIFY(name == cep->getCepCoreSchemaCep()->actionExtensions().get().actionExtension().at(0).name().c_str());
}

void TestCep::addComponentExtension()
{
	CepC2CepS* cep = new CepC2CepS();
	ComponentExtC2ComponentExtS* ext = new ComponentExtC2ComponentExtS();
	QString name = "name of the extension";
	ext->setName(name);

	cep->addComponentExtension(ext);

	QVERIFY(name == cep->getCepCoreSchemaCep()->componentExtensions().get().componentExtension().at(0).name().c_str());
}

void TestCep::addLibrary()
{
	CepC2CepS* cep = new CepC2CepS();
	LibraryC2LibraryS* lib = new LibraryC2LibraryS();
	QString name = "name of the extension";
	lib->setName(name);

	cep->addLibrary(lib);

	QVERIFY(name == cep->getCepCoreSchemaCep()->libraries().get().library().at(0).name().c_str());
}

void TestCep::addApplication()
{
	CepC2CepS* cep = new CepC2CepS();
	ApplicationC2ApplicationS* app = new ApplicationC2ApplicationS();
	QString name = "name of the extension";
	app->setName(name);

	cep->addApplication(app);

	QVERIFY(name == cep->getCepCoreSchemaCep()->applications().get().application().at(0).name().c_str());
}

/**************************************************************************************************
******************** TestActionExtension **********************************************************
**************************************************************************************************/

TestActionExtension::TestActionExtension()
{
}

TestActionExtension::~TestActionExtension()
{
}

void TestActionExtension::addDependencies1()
{
	ActionExtC2ActionExtS* actExt = new ActionExtC2ActionExtS();
	DependencyC2DependencyS* dep = new DependencyC2DependencyS();
	QString depName = "First Dependency";
	dep->setName(depName);

	actExt->addDependency(dep);

	QVERIFY(depName == actExt->getCepCoreSchemaActionExtension()->dependencies()->dependency().at(0).name().c_str());
}

void TestActionExtension::addDependencies2()
{
	ActionExtC2ActionExtS* actExt = new ActionExtC2ActionExtS();
	DependencyC2DependencyS* dep = new DependencyC2DependencyS();
	QString depName = "First Dependency";
	dep->setName(depName);

	DependencyC2DependencyS* dep2 = new DependencyC2DependencyS();
	QString depName2 = "First Dependency";
	dep2->setName(depName2);

	actExt->addDependency(dep);
	actExt->addDependency(dep2);

	QVERIFY(2 == actExt->getCepCoreSchemaActionExtension()->dependencies()->dependency().size());
}

void TestActionExtension::removeAllDependencies()
{
	ActionExtC2ActionExtS* actExt = new ActionExtC2ActionExtS();
	DependencyC2DependencyS* dep = new DependencyC2DependencyS();
	QString depName = "First Dependency";
	dep->setName(depName);

	DependencyC2DependencyS* dep2 = new DependencyC2DependencyS();
	QString depName2 = "First Dependency";
	dep2->setName(depName2);

	actExt->addDependency(dep);
	actExt->addDependency(dep2);

	actExt->removeAllDependencies();

	QVERIFY(0 == actExt->getCepCoreSchemaActionExtension()->dependencies()->dependency().size());
}

/**************************************************************************************************
******************** TestAction *******************************************************************
**************************************************************************************************/

TestAction::TestAction()
{
}
TestAction::~TestAction()
{
}

void TestAction::setName()
{
	ActionC2ActionS* action = new ActionC2ActionS();
	QString name = "A first Action";
	action->setName(name);

	QVERIFY(name == action->getCepCoreSchemaAction()->name().c_str());
}

void TestAction::setParameter1()
{
	ActionC2ActionS* action = new ActionC2ActionS();
	QString name = "A first Action";
	ParameterC2ParameterS* param = new ParameterC2ParameterS();
	QString paramName = "First parameter";
	param->setName(paramName);
	action->addParameter(param);

	QVERIFY(paramName == action->getCepCoreSchemaAction()->parameters()->parameter().at(0).name().c_str());
}

void TestAction::setParameter2()
{
	ActionC2ActionS* action = new ActionC2ActionS();
	QString name = "A first Action";
	ParameterC2ParameterS* param = new ParameterC2ParameterS();
	param->setName("First parameter");
	action->addParameter(param);

	ParameterC2ParameterS* param2 = new ParameterC2ParameterS();
	param2->setName("Second parameter");
	action->addParameter(param2);

	QVERIFY(2 == action->getCepCoreSchemaAction()->parameters()->parameter().size());

}

void TestAction::removeAllParameters()
{
	ActionC2ActionS* action = new ActionC2ActionS();
	QString name = "A first Action";
	ParameterC2ParameterS* param = new ParameterC2ParameterS();
	param->setName("First parameter");
	action->addParameter(param);

	ParameterC2ParameterS* param2 = new ParameterC2ParameterS();
	param2->setName("Second parameter");
	action->addParameter(param2);

	action->removeAllParameters();

	ParameterC2ParameterS* param3 = new ParameterC2ParameterS();
	param3->setName("Third parameter");
	action->addParameter(param3);

	QVERIFY(1 == action->getCepCoreSchemaAction()->parameters()->parameter().size());
}
void TestAction::addTag()
{
	ActionC2ActionS* action = new ActionC2ActionS();
	QString name = "A first Action";
	QString tag = "This is a tag";
	action->addTag(tag);

	QVERIFY(tag == action->getCepCoreSchemaAction()->classification().tag().at(0).c_str());
}
void TestAction::removeAllTags()
{
	ActionC2ActionS* action = new ActionC2ActionS();
	QString name = "A first Action";
	QString tag = "This is a tag";
	action->addTag(tag);
	action->removeAllTags();

	QVERIFY(0 == action->getCepCoreSchemaAction()->classification().tag().size());
}
void TestAction::setITKFilter()
{
	ActionC2ActionS* action = new ActionC2ActionS();
	QString name = "A first Action";
	QString itkFilter = "ITKFilter";
	action->setITKFilter(itkFilter);

	QVERIFY(itkFilter == action->getCepCoreSchemaAction()->classification().itkFilter().get().outputType().c_str());
}
void TestAction::setFamily()
{
	ActionC2ActionS* action = new ActionC2ActionS();
	QString name = "A first Action";
	QString family = "Family";
	action->setFamily(family);

	QVERIFY(family == action->getCepCoreSchemaAction()->classification().family().c_str());
}

/**************************************************************************************************
******************** TestComponentExtension **********************************************************
**************************************************************************************************/

TestComponentExtension::TestComponentExtension()
{
}

TestComponentExtension::~TestComponentExtension()
{
}

void TestComponentExtension::addDependencies1()
{
	ComponentExtC2ComponentExtS* actExt = new ComponentExtC2ComponentExtS();
	DependencyC2DependencyS* dep = new DependencyC2DependencyS();
	QString depName = "First Dependency";
	dep->setName(depName);

	actExt->addDependency(dep);

	QVERIFY(depName == actExt->getCepCoreSchemaComponentExtension()->dependencies()->dependency().at(0).name().c_str());
}

void TestComponentExtension::addDependencies2()
{
	ComponentExtC2ComponentExtS* actExt = new ComponentExtC2ComponentExtS();
	DependencyC2DependencyS* dep = new DependencyC2DependencyS();
	QString depName = "First Dependency";
	dep->setName(depName);

	DependencyC2DependencyS* dep2 = new DependencyC2DependencyS();
	QString depName2 = "First Dependency";
	dep2->setName(depName2);

	actExt->addDependency(dep);
	actExt->addDependency(dep2);

	QVERIFY(2 == actExt->getCepCoreSchemaComponentExtension()->dependencies()->dependency().size());
}

void TestComponentExtension::removeAllDependencies()
{
	ComponentExtC2ComponentExtS* actExt = new ComponentExtC2ComponentExtS();
	DependencyC2DependencyS* dep = new DependencyC2DependencyS();
	QString depName = "First Dependency";
	dep->setName(depName);

	DependencyC2DependencyS* dep2 = new DependencyC2DependencyS();
	QString depName2 = "First Dependency";
	dep2->setName(depName2);

	actExt->addDependency(dep);
	actExt->addDependency(dep2);

	actExt->removeAllDependencies();

	QVERIFY(0 == actExt->getCepCoreSchemaComponentExtension()->dependencies()->dependency().size());
}

/**************************************************************************************************
******************** TestComponent *******************************************************************
**************************************************************************************************/

TestComponent::TestComponent()
{
}
TestComponent::~TestComponent()
{
}

void TestComponent::setName()
{
	ComponentC2ComponentS* component = new ComponentC2ComponentS();
	QString name = "A first Component";
	component->setName(name);

	QVERIFY(name == component->getCepCoreSchemaComponent()->name().c_str());
}

void TestComponent::addProperty1()
{
	ComponentC2ComponentS* component = new ComponentC2ComponentS();
	QString name = "A first Component";
	ParameterC2ParameterS* param = new ParameterC2ParameterS();
	QString paramName = "First parameter";
	param->setName(paramName);
	component->addProperty(param);

	QVERIFY(paramName == component->getCepCoreSchemaComponent()->properties()->parameter().at(0).name().c_str());
}

void TestComponent::addProperty2()
{
	ComponentC2ComponentS* component = new ComponentC2ComponentS();
	QString name = "A first Component";
	ParameterC2ParameterS* param = new ParameterC2ParameterS();
	param->setName("First parameter");
	component->addProperty(param);

	ParameterC2ParameterS* param2 = new ParameterC2ParameterS();
	param2->setName("Second parameter");
	component->addProperty(param2);

	QVERIFY(2 == component->getCepCoreSchemaComponent()->properties()->parameter().size());

}

void TestComponent::removeAllProperties()
{
	ComponentC2ComponentS* component = new ComponentC2ComponentS();
	QString name = "A first Component";
	ParameterC2ParameterS* param = new ParameterC2ParameterS();
	param->setName("First parameter");
	component->addProperty(param);

	ParameterC2ParameterS* param2 = new ParameterC2ParameterS();
	param2->setName("Second parameter");
	component->addProperty(param2);

	component->removeAllProperties();

	ParameterC2ParameterS* param3 = new ParameterC2ParameterS();
	param3->setName("Third parameter");
	component->addProperty(param3);

	QVERIFY(1 == component->getCepCoreSchemaComponent()->properties()->parameter().size());
}
void TestComponent::setRepresentation()
{
	ComponentC2ComponentS* cmp = new ComponentC2ComponentS();
	QString str = "Representation";
	cmp->setRepresentation(str);

	QVERIFY(str == cmp->getCepCoreSchemaComponent()->representation().c_str());
}

void TestComponent::setFileSuffix()
{
	ComponentC2ComponentS* cmp = new ComponentC2ComponentS();
	QString str = ".bdl";
	cmp->setFileSuffix(str);

	QVERIFY(str == cmp->getCepCoreSchemaComponent()->fileSuffix().at(0).c_str());
}

/**************************************************************************************************
******************** TestLibrary **********************************************************
**************************************************************************************************/

TestLibrary::TestLibrary()
{
}

TestLibrary::~TestLibrary()
{
}

void TestLibrary::addDependencies1()
{
	LibraryC2LibraryS* lib = new LibraryC2LibraryS();
	DependencyC2DependencyS* dep = new DependencyC2DependencyS();
	QString depName = "First Dependency";
	dep->setName(depName);

	lib->addDependency(dep);

	QVERIFY(depName == lib->getCepCoreSchemaLibrary()->dependencies()->dependency().at(0).name().c_str());
}

void TestLibrary::addDependencies2()
{
	LibraryC2LibraryS* lib = new LibraryC2LibraryS();
	DependencyC2DependencyS* dep = new DependencyC2DependencyS();
	QString depName = "First Dependency";
	dep->setName(depName);

	DependencyC2DependencyS* dep2 = new DependencyC2DependencyS();
	QString depName2 = "First Dependency";
	dep2->setName(depName2);

	lib->addDependency(dep);
	lib->addDependency(dep2);

	QVERIFY(2 == lib->getCepCoreSchemaLibrary()->dependencies()->dependency().size());
}

void TestLibrary::removeAllDependencies()
{
	LibraryC2LibraryS* lib = new LibraryC2LibraryS();
	DependencyC2DependencyS* dep = new DependencyC2DependencyS();
	QString depName = "First Dependency";
	dep->setName(depName);

	DependencyC2DependencyS* dep2 = new DependencyC2DependencyS();
	QString depName2 = "First Dependency";
	dep2->setName(depName2);

	lib->addDependency(dep);
	lib->addDependency(dep2);

	lib->removeAllDependencies();

	QVERIFY(0 == lib->getCepCoreSchemaLibrary()->dependencies()->dependency().size());
}