#include <QtTest/QtTest>

#include "CamiTK2CepCoreSchema_unit_tests_API.h"

class CAMITK2CEPCORESCHEMA_UNIT_TESTS_API TestCep : public QObject
{
	Q_OBJECT
public:
	TestCep();
	virtual ~TestCep();
		
		private slots:
	void setName();
	void setDescription();
	void setReadMe();
	void setContact();
	void addContact();
	void removeAllContact();
	void setContactList();
	void setLicense();
	void addActionExtension();
	void addComponentExtension();
	void addLibrary();
	void addApplication();
};

class CAMITK2CEPCORESCHEMA_UNIT_TESTS_API TestActionExtension : public QObject
{
	Q_OBJECT
public:
	TestActionExtension();
	virtual ~TestActionExtension();

	private slots:
	/**
	Test if one dependency is added in the dom tree
	(test the else of the method addDependencies())
	*/
	void addDependencies1();
	/**
	Test if two dependencies are added in the dom tree
	(test the else of the method addDependencies(), and then the if)
	*/
	void addDependencies2();
	void removeAllDependencies();
};

class CAMITK2CEPCORESCHEMA_UNIT_TESTS_API TestAction : public QObject
{
	Q_OBJECT
public:
	TestAction();
	virtual ~TestAction();

	private slots:
	void setName();
	/**
	Test if One parameter is added in the dom tree
	(test the else of the method setParameter())
	*/
	void setParameter1();
	/**
	Test if two parameters are added in the dom tree
	(test the if of the method setParameter())
	*/
	void setParameter2();
	/**
	Test if all parameters added in the dom tree are removed and if it is possible to add an other one
	(when added an other one, the "if" is called, whereas it would be great that it is the "else":
	ie: the field "parameters" is not destroyed in the removeAllParameter() method, and I don't know how to destroy it. )
	*/
	void removeAllParameters();
	void addTag();
	void removeAllTags();
	void setITKFilter();
	void setFamily();
};

class CAMITK2CEPCORESCHEMA_UNIT_TESTS_API TestComponentExtension : public QObject
{
	Q_OBJECT
public:
	TestComponentExtension();
	virtual ~TestComponentExtension();

	private slots:
	/**
	Test if one dependency is added in the dom tree
	(test the else of the method addDependencies())
	*/
	void addDependencies1();
	/**
	Test if two dependencies are added in the dom tree
	(test the else of the method addDependencies(), and then the if)
	*/
	void addDependencies2();
	void removeAllDependencies();
};

class CAMITK2CEPCORESCHEMA_UNIT_TESTS_API TestComponent : public QObject
{
	Q_OBJECT
public:
	TestComponent();
	virtual ~TestComponent();

	private slots:
	void setName();
	/**
	Test if One property is added in the dom tree
	(test the else of the method addProperty())
	*/
	void addProperty1();
	/**
	Test if two properties are added in the dom tree
	(test the if of the method addProperty())
	*/
	void addProperty2();
	/**
	Test if all properties added in the dom tree are removed and if it is possible to add an other one
	(when added an other one, the "if" is called, whereas it would be great that it is the "else":
	ie: the field "properties" is not destroyed in the removeAllProperties() method, and I don't know how to destroy it. )
	*/
	void removeAllProperties();
	void setRepresentation();
	void setFileSuffix();
};

class CAMITK2CEPCORESCHEMA_UNIT_TESTS_API TestLibrary : public QObject
{
	Q_OBJECT
public:
	TestLibrary();
	virtual ~TestLibrary();

	private slots:
	/**
	Test if one dependency is added in the dom tree
	(test the else of the method addDependencies())
	*/
	void addDependencies1();
	/**
	Test if two dependencies are added in the dom tree
	(test the else of the method addDependencies(), and then the if)
	*/
	void addDependencies2();
	void removeAllDependencies();
};