//#include "stdafx.h"
#include "CppUnitTest.h"
#include "../libraries/camitk2cepcoreschema/CepC2CepS.h"
#include "../libraries/camitk2cepcoreschema/ActionExtC2ActionExtS.h"
#include "../libraries/camitk2cepcoreschema/ActionC2ActionS.h"
#include "../libraries/camitk2cepcoreschema/ParameterC2ParameterS.h"
#include "../libraries/camitk2cepcoreschema/DependencyC2DependencyS.h"
#include "../libraries/camitk2cepcoreschema/ComponentExtC2ComponentExtS.h"
#include "../libraries/camitk2cepcoreschema/ComponentC2ComponentS.h"
#include "../libraries/camitk2cepcoreschema/LibraryC2LibraryS.h"
#include "../libraries/camitk2cepcoreschema/ApplicationC2ApplicationS.h"
#include "Cep.hxx"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(Cep)
	{
	public:

		//TEST_METHOD(TestMethod1)
		//{
		//	CepC2CepS* cep = new CepC2CepS();
		//	Assert::AreEqual(
		//		// Expected value:
		//		3.0,
		//		// Actual value:
		//		cep->getIntValue(3),
		//		// Tolerance:
		//		0.01,
		//		// Message:
		//		L"Basic test failed",
		//		// Line number - used if there is no PDB file:
		//		LINE_INFO());
		//}

		TEST_METHOD(SetName)
		{
			CepC2CepS* cep = new CepC2CepS();
			QString name = "A first CEP";
			cep->setName(name);
			Assert::AreEqual(
				// Expected value:
				name.toStdString().c_str(),
				// Actual value:
				cep->getCepCoreSchemaCep()->name().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(SetDescription)
		{
			CepC2CepS* cep = new CepC2CepS();
			QString str = "CEP description";
			cep->setDescription(str);
			Assert::AreEqual(
				// Expected value:
				str.toStdString().c_str(),
				// Actual value:
				cep->getCepCoreSchemaCep()->description().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(SetReadMe)
		{
			CepC2CepS* cep = new CepC2CepS();
			QString str = "ReadMe";
			cep->setReadMe(str);
			Assert::AreEqual(
				// Expected value:
				str.toStdString().c_str(),
				// Actual value:
				cep->getCepCoreSchemaCep()->readme().get().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(SetContact)
		{
			CepC2CepS* cep = new CepC2CepS();
			QString str = "titi.toto@imag.fr";
			cep->setContact(str);
			Assert::AreEqual(
				// Expected value:
				str.toStdString().c_str(),
				// Actual value:
				cep->getCepCoreSchemaCep()->contact().email().at(0).c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(AddContact)
		{
			CepC2CepS* cep = new CepC2CepS();
			QString str = "bili.toto@imag.fr";
			cep->addContact(str);
			Assert::AreEqual(
				// Expected value:
				str.toStdString().c_str(),
				// Actual value:
				cep->getCepCoreSchemaCep()->contact().email().back().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(RemoveAllContact)
		{
			CepC2CepS* cep = new CepC2CepS();
			QString str = "bili.toto@imag.fr";
			cep->addContact(str);
			cep->removeAllContact();
			Assert::AreEqual(
				// Expected value:
				0,
				// Actual value:
				cep->getCepCoreSchemaCep()->contact().email().size(),
				// Tolerance:
				0.01,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(SetContactList)
		{
			CepC2CepS* cep = new CepC2CepS();
			QStringList str("bili.toto@imag.fr");
			str.append("tata.tutu@imag.fr");
			str.append("polo.pili@imag.fr");
			cep->setContact(str);
			Assert::AreEqual(
				// Expected value:
				str.size(),
				// Actual value:
				cep->getCepCoreSchemaCep()->contact().email().size(),
				// Tolerance:
				0.01,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(SetLicence)
		{
			CepC2CepS* cep = new CepC2CepS();
			QString str = "This is the licence";
			cep->setLicence(str);
			Assert::AreEqual(
				// Expected value:
				str.toStdString().c_str(),
				// Actual value:
				cep->getCepCoreSchemaCep()->copyright().get().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(AddActionExtension)
		{
			CepC2CepS* cep = new CepC2CepS();
			ActionExtC2ActionExtS* actExt = new ActionExtC2ActionExtS();

			cep->addActionExtension(actExt);
			Assert::AreEqual(
				// Expected value:
				actExt->getCepCoreSchemaActionExtension()->name().c_str(),
				// Actual value:
				cep->getCepCoreSchemaCep()->actionExtensions().get().actionExtension().at(0).name().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(AddComponentExtension)
		{
			CepC2CepS* cep = new CepC2CepS();
			ComponentExtC2ComponentExtS* ext = new ComponentExtC2ComponentExtS();

			cep->addComponentExtension(ext);
			Assert::AreEqual(
				// Expected value:
				ext->getCepCoreSchemaComponentExtension()->name().c_str(),
				// Actual value:
				cep->getCepCoreSchemaCep()->componentExtensions().get().componentExtension().at(0).name().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(AddLibrary)
		{
			CepC2CepS* cep = new CepC2CepS();
			LibraryC2LibraryS* lib = new LibraryC2LibraryS();

			cep->addLibrary(lib);
			Assert::AreEqual(
				// Expected value:
				lib->getCepCoreSchemaLibrary()->name().c_str(),
				// Actual value:
				cep->getCepCoreSchemaCep()->libraries().get().library().at(0).name().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(AddApplication)
		{
			CepC2CepS* cep = new CepC2CepS();
			ApplicationC2ApplicationS* app = new ApplicationC2ApplicationS();

			cep->addApplication(app);
			Assert::AreEqual(
				// Expected value:
				app->getCepCoreSchemaApplication()->name().c_str(),
				// Actual value:
				cep->getCepCoreSchemaCep()->applications().get().application().at(0).name().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}
	};

	TEST_CLASS(ActionExtension)
	{
	public:
		/**
		Test if One dependency is added in the dom tree
		(test the else of the method addDependencies())
		*/
		TEST_METHOD(AddDependencies1)
		{
			ActionExtC2ActionExtS* actExt = new ActionExtC2ActionExtS();
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			QString depName = "First Dependency";
			dep->setName(depName);

			actExt->addDependency(dep);

			Assert::AreEqual(
				// Expected value:
				depName.toStdString().c_str(),
				// Actual value:
				actExt->getCepCoreSchemaActionExtension()->dependencies()->dependency().at(0).name().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		/**
		Test if two dependencies are added in the dom tree
		(test the else of the method addDependencies(), and then the if)
		*/
		TEST_METHOD(AddDependencies2)
		{
			ActionExtC2ActionExtS* actExt = new ActionExtC2ActionExtS();
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			QString depName = "First Dependency";
			dep->setName(depName);

			DependencyC2DependencyS* dep2 = new DependencyC2DependencyS();
			QString depName2 = "First Dependency";
			dep2->setName(depName2);

			actExt->addDependency(dep);
			actExt->addDependency(dep2);

			Assert::AreEqual(
				// Expected value:
				2,
				// Actual value:
				actExt->getCepCoreSchemaActionExtension()->dependencies()->dependency().size(),
				// tolerance:
				0.0,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(RemoveAllDependencies)
		{
			ActionExtC2ActionExtS* actExt = new ActionExtC2ActionExtS();
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			QString depName = "First Dependency";
			dep->setName(depName);

			DependencyC2DependencyS* dep2 = new DependencyC2DependencyS();
			QString depName2 = "First Dependency";
			dep2->setName(depName2);

			actExt->addDependency(dep);
			actExt->addDependency(dep2);

			actExt->removeAllDependencies();

			Assert::AreEqual(
				// Expected value:
				0,
				// Actual value:
				actExt->getCepCoreSchemaActionExtension()->dependencies()->dependency().size(),
				// tolerance:
				0.0,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}
	};

	TEST_CLASS(Action)
	{
	public:
		TEST_METHOD(SetName)
		{
			ActionC2ActionS* action = new ActionC2ActionS();
			QString name = "A first Action";
			action->setName(name);
			Assert::AreEqual(
				// Expected value:
				name.toStdString().c_str(),
				// Actual value:
				action->getCepCoreSchemaAction()->name().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		/**
		Test if One parameter is added in the dom tree
		(test the else of the method setParameter())
		*/
		TEST_METHOD(SetParameter1)
		{
			ActionC2ActionS* action = new ActionC2ActionS();
			QString name = "A first Action";
			ParameterC2ParameterS* param = new ParameterC2ParameterS();
			param->setName("First parameter");
			action->addParameter(param);
			Assert::AreEqual(
				// Expected value:
				param->getCepCoreSchemaParameter()->name().c_str(),
				// Actual value:
				action->getCepCoreSchemaAction()->parameters()->parameter().at(0).name().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		/**
		Test if two parameters are added in the dom tree
		(test the if of the method setParameter())
		*/
		TEST_METHOD(SetParameter2)
		{
			ActionC2ActionS* action = new ActionC2ActionS();
			QString name = "A first Action";
			ParameterC2ParameterS* param = new ParameterC2ParameterS();
			param->setName("First parameter");
			action->addParameter(param);

			ParameterC2ParameterS* param2 = new ParameterC2ParameterS();
			param2->setName("Second parameter");
			action->addParameter(param2);
			Assert::AreEqual(
				// Expected value:
				2,
				// Actual value:
				action->getCepCoreSchemaAction()->parameters()->parameter().size(),
				// Tolerance:
				0.00,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		/**
		Test if all parameters added in the dom tree are removed and if it is possible to add an other one
		(when added an other one, the "if" is called, whereas it would be great that it is the "else": 
		ie: the field "parameters" is not destroyed in the removeAllParameter() method, and I don't know how to destroy it. )
		*/
		TEST_METHOD(RemoveAllParameters)
		{
			ActionC2ActionS* action = new ActionC2ActionS();
			QString name = "A first Action";
			ParameterC2ParameterS* param = new ParameterC2ParameterS();
			param->setName("First parameter");
			action->addParameter(param);

			ParameterC2ParameterS* param2 = new ParameterC2ParameterS();
			param2->setName("Second parameter");
			action->addParameter(param2);

			action->removeAllParameters();

			ParameterC2ParameterS* param3 = new ParameterC2ParameterS();
			param3->setName("Third parameter");
			action->addParameter(param3);

			Assert::AreEqual(
				// Expected value:
				1,
				// Actual value:
				action->getCepCoreSchemaAction()->parameters()->parameter().size(),
				// Tolerance:
				0.00,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(AddTag)
		{
			ActionC2ActionS* action = new ActionC2ActionS();
			QString name = "A first Action";
			QString tag = "This is a tag";
			action->addTag(tag);
			Assert::AreEqual(
				// Expected value:
				tag.toStdString().c_str(),
				// Actual value:
				action->getCepCoreSchemaAction()->classification().tag().at(0).c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(RemoveAllTags)
		{
			ActionC2ActionS* action = new ActionC2ActionS();
			QString name = "A first Action";
			QString tag = "This is a tag";
			action->addTag(tag);
			action->removeAllTags();
			Assert::AreEqual(
				// Expected value:
				0,
				// Actual value:
				action->getCepCoreSchemaAction()->classification().tag().size(),
				// Tolerance:
				0.00,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(SetITKFilter)
		{
			ActionC2ActionS* action = new ActionC2ActionS();
			QString name = "A first Action";
			QString itkFilter = "ITKFilter";
			action->setITKFilter(itkFilter);
			Assert::AreEqual(
				// Expected value:
				itkFilter.toStdString().c_str(),
				// Actual value:
				action->getCepCoreSchemaAction()->classification().itkFilter().get().outputType().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(SetFamily)
		{
			ActionC2ActionS* action = new ActionC2ActionS();
			QString name = "A first Action";
			QString family = "Family";
			action->setFamily(family);
			Assert::AreEqual(
				// Expected value:
				family.toStdString().c_str(),
				// Actual value:
				action->getCepCoreSchemaAction()->classification().family().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}
	};

	TEST_CLASS(ComponentExtension)
	{
	public:
		/**
		Test if One dependency is added in the dom tree
		(test the else of the method addDependencies())
		*/
		TEST_METHOD(AddDependencies1)
		{
			ComponentExtC2ComponentExtS* ext = new ComponentExtC2ComponentExtS();
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			QString depName = "First Dependency";
			dep->setName(depName);

			ext->addDependency(dep);

			Assert::AreEqual(
				// Expected value:
				depName.toStdString().c_str(),
				// Actual value:
				ext->getCepCoreSchemaComponentExtension()->dependencies()->dependency().at(0).name().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		/**
		Test if two dependencies are added in the dom tree
		(test the else of the method addDependencies(), and then the if)
		*/
		TEST_METHOD(AddDependencies2)
		{
			ComponentExtC2ComponentExtS* ext = new ComponentExtC2ComponentExtS();
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			QString depName = "First Dependency";
			dep->setName(depName);

			DependencyC2DependencyS* dep2 = new DependencyC2DependencyS();
			QString depName2 = "First Dependency";
			dep2->setName(depName2);

			ext->addDependency(dep);
			ext->addDependency(dep2);

			Assert::AreEqual(
				// Expected value:
				2,
				// Actual value:
				ext->getCepCoreSchemaComponentExtension()->dependencies()->dependency().size(),
				// tolerance:
				0.0,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(RemoveAllDependencies)
		{
			ComponentExtC2ComponentExtS* ext = new ComponentExtC2ComponentExtS();
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			QString depName = "First Dependency";
			dep->setName(depName);

			DependencyC2DependencyS* dep2 = new DependencyC2DependencyS();
			QString depName2 = "First Dependency";
			dep2->setName(depName2);

			ext->addDependency(dep);
			ext->addDependency(dep2);

			ext->removeAllDependencies();

			Assert::AreEqual(
				// Expected value:
				0,
				// Actual value:
				ext->getCepCoreSchemaComponentExtension()->dependencies()->dependency().size(),
				// tolerance:
				0.0,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}
	};

	TEST_CLASS(Component)
	{
	public:
		TEST_METHOD(SetName)
		{
			ComponentC2ComponentS* cmp = new ComponentC2ComponentS();
			QString name = "A first Comp";
			cmp->setName(name);
			Assert::AreEqual(
				// Expected value:
				name.toStdString().c_str(),
				// Actual value:
				cmp->getCepCoreSchemaComponent()->name().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		/**
		Test if One parameter is added in the dom tree
		(test the else of the method setParameter())
		*/
		TEST_METHOD(AddProperty1)
		{
			ComponentC2ComponentS* cmp = new ComponentC2ComponentS();
			QString name = "A first COMP";
			ParameterC2ParameterS* param = new ParameterC2ParameterS();
			param->setName("First parameter");
			cmp->addProperty(param);
			Assert::AreEqual(
				// Expected value:
				param->getCepCoreSchemaParameter()->name().c_str(),
				// Actual value:
				cmp->getCepCoreSchemaComponent()->properties()->parameter().at(0).name().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		/**
		Test if two parameters are added in the dom tree
		(test the if of the method setParameter())
		*/
		TEST_METHOD(AddProperty2)
		{
			ComponentC2ComponentS* cmp = new ComponentC2ComponentS();
			QString name = "A first Comp";
			ParameterC2ParameterS* param = new ParameterC2ParameterS();
			param->setName("First parameter");
			cmp->addProperty(param);

			ParameterC2ParameterS* param2 = new ParameterC2ParameterS();
			param2->setName("Second parameter");
			cmp->addProperty(param2);
			Assert::AreEqual(
				// Expected value:
				2,
				// Actual value:
				cmp->getCepCoreSchemaComponent()->properties()->parameter().size(),
				// Tolerance:
				0.00,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		/**
		Test if all parameters added in the dom tree are removed and if it is possible to add an other one
		(when added an other one, the "if" is called, whereas it would be great that it is the "else":
		ie: the field "parameters" is not destroyed in the removeAllParameter() method, and I don't know how to destroy it. )
		*/
		TEST_METHOD(RemoveAllProperties)
		{
			ComponentC2ComponentS* cmp = new ComponentC2ComponentS();
			QString name = "A first Action";
			ParameterC2ParameterS* param = new ParameterC2ParameterS();
			param->setName("First parameter");
			cmp->addProperty(param);

			ParameterC2ParameterS* param2 = new ParameterC2ParameterS();
			param2->setName("Second parameter");
			cmp->addProperty(param2);

			cmp->removeAllProperties();

			ParameterC2ParameterS* param3 = new ParameterC2ParameterS();
			param3->setName("Third parameter");
			cmp->addProperty(param3);

			Assert::AreEqual(
				// Expected value:
				1,
				// Actual value:
				cmp->getCepCoreSchemaComponent()->properties()->parameter().size(),
				// Tolerance:
				0.00,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(SetRepresentation)
		{
			ComponentC2ComponentS* cmp = new ComponentC2ComponentS();
			QString str = "Representation";
			cmp->setRepresentation(str);

			Assert::AreEqual(
				// Expected value:
				str.toStdString().c_str(),
				// Actual value:
				cmp->getCepCoreSchemaComponent()->representation().c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		TEST_METHOD(SetFileSuffix)
		{
			ComponentC2ComponentS* cmp = new ComponentC2ComponentS();
			QString str = ".bdl";
			cmp->setFileSuffix(str);

			Assert::AreEqual(
				// Expected value:
				str.toStdString().c_str(),
				// Actual value:
				cmp->getCepCoreSchemaComponent()->fileSuffix().at(0).c_str(),
				// ignoreCase:
				true,
				// Message:
				L"Basic test failed",
				// Line number - used if there is no PDB file:
				LINE_INFO());
		}

		
	};
}